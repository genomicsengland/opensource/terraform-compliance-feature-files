# terraform-compliance features

This repository contains all terraform-compliance features.

[terraform-compliance](https://terraform-compliance.com/) utilises [radish](http://radish-bdd.io/)
to handle BDD directives. BDD is used in many development practices from end-to-end testing to FrontEnd testing,
provides easy-to-understand context that is self-descriptive and easy-to-understand for someone that is reading the
test results.

In BDD, every feature file will have 3 components ;

* Feature
* Scenario/Scenario Outline
* Steps

Please note that `terraform-compliance` has fixed steps already defined within the tool.

In order to create new test, please add `.feature` file into `features` folder.

This is an example of a feature file that enforces usage od specific tags:

```gherkin
Feature: Tags should be used to help manage, identify, organize, search for, and filter resources.

Scenario Outline: Ensure that specific tags are defined
    Given I have resource that supports tags defined
    When it contains tags
    Then it must contain <tags>
    And its value must not be null

    Examples:
      | Squad       |
      | Tribe       |
      | Application |
      | Environment |
```
References:

* [terraform-compliance examples](https://terraform-compliance.com/pages/Examples/)
* [radish feature files tutorial](https://radish.readthedocs.io/en/stable/tutorial.html#feature-files)
