@provider_level_map_migration_tags
Feature: Provider Level MAP Migration Tags
  Check if MAP migration tag aws-migration-project-id is set on provider level and match the required value

@case_sensitive
  Scenario Outline: Ensure that specific tags are defined on provider level
    Given I have resource that supports tags defined
    When it has tags_all
    Then it must contain tags_all
    Then it must contain "<tags>"
    And its value must match the "<value>" regex

    Examples:
      | tags                     | value    |
      | aws-migration-project-id | MPE17143 |
