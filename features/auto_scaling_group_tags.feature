@auto_scaling_group_tags
Feature: Auto Scaling Group Tags
  Check if aws_autoscaling_group has tags that match tag policy

@case_sensitive
  Scenario Outline: Ensure that specific tags are defined
    Given I have resource that supports tags defined
    When its type is aws_autoscaling_group
    Then it must contain <tags>
    And its value must match the "<value>" regex

    Examples:
      | tags        | value                                               |
      | Squad       | ^(Assurance\|Commercial Procurement\|Core Healthcare\|Estates\|Executive Support\|Finance\|People\|Performance\|Procurement\|Quality\|Business Systems\|Comms And Marketing\|Engagement\|Ethics And IG\|100k Delivery\|Bio Pipeline Core\|Bio Pipeline Delivery\|Bio Pipeline Discovery\|Bioinformatics Pipeline\|Clinical Data Management\|Future Informatics\|Infrastructure Services\|Interpretation Platform\|Knowledge Management\|Logistics\|Long Read\|Test Ordering And Tracking\|Bioinformatics Consulting\|Cloud RE\|Cohort Management\|Cohort Tools\|Core Research Tools\|COVID Programme\|Diversity\|Partnership Development\|RE 1.0\|Research Data Layer Squad\|Research Data Products\|Research Management\|Scientific R And D\|Cloud Enablement\|Core Tech\|Enterprise Data\|HPC And Data Centres\|The GEL Way\|Sample And Data Management\|Security And Networking\|Site Reliability Engineering\|Newborns Intake\|Newborns Evaluation\|Newborns CRM)$ |
      | Tribe       | ^(Empowering Our People\|Engaging To Build Trust\|Evolving Genomic Healthcare\|Research Ecosystem\|Scalable Tech\|The GEL Way)$ |
      | Application | .+                                                  |
      | Environment | ^(Prod\|Preprod\|Uat\|Test\|Dev\|Staging\|Sandbox)$ |
