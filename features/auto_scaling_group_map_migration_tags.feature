@auto_scaling_group_map_migration_tags
Feature: Auto Scaling Group MAP Migration Tags
  Check if aws_autoscaling_group MAP migration tag aws-migration-project-id is set and match the required value

@case_sensitive
  Scenario Outline: Ensure that specific tags are defined
    Given I have resource that supports tags defined
    When its type is aws_autoscaling_group
    Then it must contain <tags>
    And its value must match the "<value>" regex

    Examples:
      | tags                     | value    |
      | aws-migration-project-id | MPE17143 |
